# Tampilan Aplikasi
Desain dibuat memakai Bootstrap 4 Murni.
Ada sebagian desain ambil dari referensi (kaijabar.or.id)

## 1. Beranda
![Beranda](ssaplikasi/1.PNG)

## 2. Tentang Kami
![Beranda](ssaplikasi/about.PNG)

## 3. Ketua
![Beranda](ssaplikasi/2.PNG)

## 4. Informasi
![Beranda](ssaplikasi/3.PNG)

## 5. Layanan
![Beranda](ssaplikasi/4.PNG)

## 6. Footer
![Beranda](ssaplikasi/5.PNG)

